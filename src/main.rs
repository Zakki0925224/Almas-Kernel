#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(almas_kernel::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use core::panic::PanicInfo;
use almas_kernel::{arch::{allocator, memory, task::{Task, executor::Executor}}, drivers::serial::keyboard, println};
use bootloader::{BootInfo, entry_point};
use x86_64::VirtAddr;

entry_point!(kernel_main);

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> !
{
    println!("{}", info);
    almas_kernel::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> !
{
    almas_kernel::test_panic_handler(info);
}

fn kernel_main(boot_info: &'static BootInfo) -> !
{
    println!("Hello! Almas Kernel");

    // kernel initializer
    almas_kernel::init();

    // initialize memory allocator
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };
    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");
    println!("Initialized memory allocator");

    let mut executor = Executor::new();
    executor.spawn(Task::new(keyboard::print_keypresses()));
    executor.run();

    #[cfg(test)]
    test_main();

    println!("Welcome to Almas Kernel. Successful startup!");
    // start hlt loop
    almas_kernel::hlt_loop();
}