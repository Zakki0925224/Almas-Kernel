use core::{fmt::{self, Write}, ptr::{read_volatile, write_volatile}};
use lazy_static::lazy_static;
use spin::Mutex;
use x86_64::instructions::interrupts;

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;
const VGA_MEM: u32 = 0xb8000;

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color
{
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Writer
{
    color_code: u8,
    x_position: usize,
    y_position: usize
}

impl Writer
{
    pub fn new(fore_color: Color, back_color: Color) -> Self
    {
        return Writer{color_code: (back_color as u8) << 4 | (fore_color as u8), x_position: 1, y_position: 1};
    }

    pub fn set_color(&mut self, fore_color: Color, back_color: Color)
    {
        self.color_code = (back_color as u8) << 4 | (fore_color as u8);
    }

    fn write_data(&mut self, data: u8, offset: isize)
    {
        unsafe
        {
            let buffer = VGA_MEM as *mut u8;
            write_volatile(buffer.offset(offset), data);
            write_volatile(buffer.offset(offset + 1), self.color_code);
        }
    }

    fn read_data(&mut self, offset: isize) -> u8
    {
        unsafe
        {
            let buffer = VGA_MEM as *mut u8;
            return read_volatile(buffer.offset(offset));
        }
    }

    pub fn write_char(&mut self, c: char)
    {
        if c == '\n'
        {
            self.new_line();
        }
        else
        {
            let offset = (((BUFFER_WIDTH * (self.y_position - 1)) + self.x_position - 1) * 2) as isize;
            self.write_data(c as u8, offset);

            self.move_position(1);
        }
    }

    pub fn write_string(&mut self, s: &str)
    {
        for c in s.chars()
        {
            self.write_char(c);
        }
    }

    fn new_line(&mut self)
    {
        self.move_position(BUFFER_WIDTH - self.x_position + 1);
    }

    fn move_position(&mut self, move_size: usize)
    {
        self.x_position += move_size;

        if self.x_position > BUFFER_WIDTH
        {
            self.x_position = 1;
            self.y_position += 1;
        }

        if self.y_position > BUFFER_HEIGHT
        {
            self.buffer_scroll();
            self.x_position = 1;
            self.y_position = BUFFER_HEIGHT;
        }
    }

    fn buffer_scroll(&mut self)
    {
        for i in BUFFER_WIDTH..(BUFFER_WIDTH * BUFFER_HEIGHT * 2)
        {
            let buf = self.read_data((i * 2) as isize);
            self.write_data(buf, ((i - BUFFER_WIDTH) * 2) as isize);
        }
    }

    pub fn cls(&mut self)
    {
        for i in 0..BUFFER_WIDTH * BUFFER_HEIGHT
        {
            self.write_data(0, (i * 2) as isize);
            self.write_data(0, (i * 2 + 1) as isize);
        }
    }
}

lazy_static!
{
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer::new(Color::White, Color::Black));
}

impl fmt::Write for Writer
{
    fn write_str(&mut self, s: &str) -> fmt::Result
    {
        self.write_string(s);
        Ok(())
    }
}

#[macro_export]
macro_rules! print
{
    ($($arg:tt)*) => ($crate::arch::vga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println
{
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments)
{
    interrupts::without_interrupts(|| { WRITER.lock().write_fmt(args).unwrap(); });
}

#[test_case]
fn test_println_simple()
{
    println!("test_println_simple output");
}

#[test_case]
fn test_println_many()
{
    for _ in 0..200 { println!("test_println_many output"); }
}