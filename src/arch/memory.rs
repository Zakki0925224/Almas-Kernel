use bootloader::bootinfo::{MemoryMap, MemoryRegionType};
use x86_64::{PhysAddr, VirtAddr, registers::control::Cr3, structures::paging::{FrameAllocator, Mapper, OffsetPageTable, Page, PageTable, PageTableFlags, PhysFrame, Size4KiB, page_table::FrameError}};

pub unsafe fn init(physical_memory_offset: VirtAddr) -> OffsetPageTable<'static>
{
    return OffsetPageTable::new(active_level4_table(physical_memory_offset), physical_memory_offset);
}

// 有効なレベル4テーブルへの可変参照を返す
// &mut参照のエイリアスを回避するために一度だけ呼び出す必要がある（未定義動作）
unsafe fn active_level4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable
{
    let (level4_table_frame, _) = Cr3::read();
    let phys = level4_table_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_table_ptr: *mut PageTable = virt.as_mut_ptr();

    return &mut *page_table_ptr;
}

// 指定された仮想アドレスをマッピングされた物理アドレスに変換
// マッピングされていない場合はNoneを返す
pub unsafe fn translate_addr(addr: VirtAddr, physical_memory_offset: VirtAddr) -> Option<PhysAddr>
{
    return translate_addr_inner(addr, physical_memory_offset);
}

fn translate_addr_inner(addr: VirtAddr, physical_memory_offset: VirtAddr) -> Option<PhysAddr>
{
    let (level4_table_frame, _) = Cr3::read();
    let table_indexes =
    [
        addr.p4_index(),
        addr.p3_index(),
        addr.p2_index(),
        addr.p1_index()
    ];

    let mut frame = level4_table_frame;

    // マルチレベルテーブルを横断
    for &index in &table_indexes
    {
        let virt = physical_memory_offset + frame.start_address().as_u64();
        let table_ptr: *const PageTable = virt.as_ptr();
        let table = unsafe { &*table_ptr };

        let entry = &table[index];
        frame = match entry.frame()
        {
            Ok(frame) => frame,
            Err(FrameError::FrameNotPresent) => return None,
            Err(FrameError::HugeFrame) => panic!("huge pages not supported")
        };
    }

    return Some(frame.start_address() + u64::from(addr.page_offset()));
}

pub fn create_example_mapping(page: Page,
                              mapper: &mut OffsetPageTable,
                              frame_allocator: &mut impl FrameAllocator<Size4KiB>)
{
    let frame = PhysFrame::containing_address(PhysAddr::new(0xb8000));
    let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;

    let map_to_result = unsafe { mapper.map_to(page, frame, flags, frame_allocator) };
    map_to_result.expect("map_to failed").flush();
}

pub struct EmptyFrameAllocator;

// ブートローダーのメモリマップから使用可能なフレームを返すFrameAllocator
pub struct BootInfoFrameAllocator
{
    memory_map: &'static MemoryMap,
    next: usize
}

impl BootInfoFrameAllocator
{
    pub unsafe fn init(memory_map: &'static MemoryMap) -> Self
    {
        return BootInfoFrameAllocator { memory_map, next: 0 };
    }

    // メモリマップで指定された使用可能なフレームのイテレータを返す
    fn usable_frames(&self) -> impl Iterator<Item = PhysFrame>
    {
        let regions = self.memory_map.iter();
        let usable_regions = regions.filter(|r| r.region_type == MemoryRegionType::Usable);
        let addr_ranges = usable_regions.map(|r| r.range.start_addr()..r.range.end_addr());
        let frame_addresses = addr_ranges.flat_map(|r| r.step_by(4096));

        return frame_addresses.map(|addr| PhysFrame::containing_address(PhysAddr::new(addr)));
    }
}

unsafe impl FrameAllocator<Size4KiB> for EmptyFrameAllocator
{
    fn allocate_frame(&mut self) -> Option<PhysFrame<Size4KiB>>
    {
        return None;
    }
}

unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator
{
    fn allocate_frame(&mut self) -> Option<PhysFrame>
    {
        let frame = self.usable_frames().nth(self.next);
        self.next += 1;
        return frame;
    }
}